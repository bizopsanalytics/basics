select email, 
        case 
        when    email like '%gmail%' 
                or email like '%hotmail%' 
                or email like '%yahoo%' 
                or email like '%live.%'
                or email like '%mail.com%'
                or email like '%rocketmail%'
                or email like '%inbox.com%'
                then email 
        else  REGEXP_SUBSTR(email,'[^@]*$')
        end as email_domain
 from staging_hipchat_user_safe 