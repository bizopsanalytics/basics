--sen count

select  year(cast(eval_start_date as date)),
        month(cast(eval_start_date as date)),
        --platform,
        --base_product, 
        count(distinct eval_sen) as eval_sen     
from public.license
where (base_product like '%JIRA%' or base_product like '%Confluence%')
and base_product not in ('JIRA Agile')
and platform = 'Cloud'
group by 1,2--,3,4
order by 1,2--,3,4

;
-- tech email count

with mindates as
(
        select  tech_email,
                min(eval_start_date) as min_date
        from public.license
        group by 1
)

select  year(cast(a.eval_start_date as date)),
        month(cast(a.eval_start_date as date)),
        --platform,
        --base_product, 
        count(distinct a.eval_sen) as eval_sen,
        count(distinct a.tech_email) as tech_email      
from public.license as a
left join mindates as b on a.tech_email = b.tech_email
where (base_product like '%JIRA%' or base_product like '%Confluence%')
and base_product not in ('JIRA Agile')
and platform = 'Cloud'
and a.eval_start_date = b.min_date    
group by 1,2--,3,4
order by 1,2--,3,4

;

-- conversions
select  financial_year,
        count(distinct sen)
from public.sale
where sale_type in ('New to New', 'New to Existing')
and (base_product like '%JIRA%' or base_product like '%Confluence%')
and base_product not in ('JIRA Agile')
and platform = 'Cloud'
group by 1

;
--starter conversion

with minstart as 
(
        select  sen, 
                min(date) as min_date                
        from public.sale         
        where sale_type = 'Starter'
        group by 1
  )              
select  financial_year,
        count(distinct a.sen)
from public.sale as a
left join minstart as b on a.sen = b.sen
where(base_product like '%JIRA%' or base_product like '%Confluence%')
and base_product not in ('JIRA Agile')
and platform = 'Cloud'
and date = b.min_date
group by 1