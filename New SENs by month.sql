with first_date as 
(
select sen, 
        min(date) as min_date
from public.sale 
--where base_product = 'Bitbucket'
where platform = 'Cloud'
group by 1
)
select  a.month,
        a.base_product, 
        count (distinct a.sen)
from public.sale as a
left join first_date as b on a.sen = b.sen
--where base_product = 'Bitbucket'
where platform = 'Cloud'
and a.date = b.min_date
and a.financial_year = 'FY2016'
group by 1,2
order by 1,2