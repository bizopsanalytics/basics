with all_active as
(
select a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain 
from    model.fact_license_active as a
join    model.dim_product as b on a.product_id = b.product_id
join    model.dim_license as c on a.license_id = c.license_id
join    model.dim_customer as d on a.customer_id = d.customer_id
where   a.date_id = 20161231
and     c.level in ('Starter')   
),
domain_own as 
(
select  smart_domain, 
        platform, 
        base_product,   
        count(distinct sen) as sens
from    all_active
group by 1,2,3
)
select  platform, 
        base_product, 
        sens, 
        count(smart_domain) as domains
from    domain_own
--where base_product in ('Confluence','JIRA Software')
group by 1,2,3
order by 4 desc