
--JSD customers as a percentage of all official customers
select
    fiscal_year_qtr
    , count(distinct case when jira_service_desk then customer_id else null end) as jsd_customers
    , count(distinct customer_id) as total_customers
    , cast(count(distinct case when jira_service_desk then customer_id else null end) as double) / cast(count(distinct customer_id) as double) as pct_core_of_all
from model.fact_customer_active
join model.dim_product_ownership on full_product_ownership_id=product_ownership_id
join model.dim_date using (Date_id)
where last_day_of_quarter and qualifies_as_full_customer=1
group by 1
order by 1 desc

;
-- marketplace addon
select
  fiscal_year_qtr,
  count(distinct customer_id) as customers,
  count(distinct case when marketplace_addon and cloud = true then customer_id end) as marketplace_customers_cloud,
  count(distinct case when marketplace_addon and server = true then customer_id end) as marketplace_customers_server,
  cast(count(distinct case when marketplace_addon and cloud = true then customer_id end) as double) / cast(count(distinct customer_id) as double) as pct_marketplace_cloud,
  cast(count(distinct case when marketplace_addon and server = true then customer_id end) as double) / cast(count(distinct customer_id) as double) as pct_marketplace_server
from model.fact_customer_active
join model.dim_product_ownership on full_product_ownership_id = product_ownership_id
join model.dim_date using (Date_id)
where qualifies_as_full_customer = 1 and last_day_of_quarter
group by 1
order by 1

