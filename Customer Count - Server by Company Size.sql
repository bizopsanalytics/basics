select  b.company_size, 
        count(distinct a.tech_email_domain) as cust_count
from public.license as a
left join zone_bizops.customer_size as b on a.tech_email_domain = b.email_domain
where a.paid_license_end_date > '2016-06-30'
and a.last_paid_license_level = 'Full'
and a.platform = 'Server'
group by 1
order by 1 asc
