

 --cloud landing product
 
 with first_purchase as
       (
        select  email_domain,
                min(date) as min_date
        from    public.sale
        where     platform = 'Cloud'
        group by 1
       ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20160930
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat',
                                        'Marketplace Addon'
                                   )     
        ),
 first_product as
        (
        select  a.smart_domain, 
                b.base_product,
                b.platform
        from    all_cloud_paid as a
        left join public.sale as b on a.smart_domain = b.email_domain
        left join first_purchase as c on b.email_domain = c.email_domain       
        where   b.date = c.min_date
         and b.base_product in (
                                        'JIRA',
                                        'JIRA Core',
                                        'JIRA Software',
                                        'JIRA Service Desk',
                                        'Confluence',
                                        'Bitbucket',
                                        'HipChat',
                                        'Marketplace Addon'
                                   )    
        group by 1,2,3
        ),
        land_product as
        ( 
                select  smart_domain, 
                        count(distinct base_product) as land_count
                from    first_product
                group by 1
        )
                       select  case 
                        -- 1 product only
                        when a.land_count = 1 and b.base_product in ('HipChat') then 1
                        when a.land_count = 1 and b.base_product in ('Confluence') then 2
                        when a.land_count = 1 and b.base_product in ('JIRA') then 3
                        when a.land_count = 1 and b.base_product in ('JIRA Software') then 4
                        when a.land_count = 1 and b.base_product in ('JIRA Service Desk') then 5
                        when a.land_count = 1 and b.base_product in ('JIRA Core') then 6
                        when a.land_count = 1 and b.base_product in ('Bitbucket') then 7
                        -- Hipchat based 2 combos
                        when a.land_count = 2 and b.base_product in ('HipChat','Confluence') then 8 
                        when a.land_count = 2 and b.base_product in ('HipChat','Bitbucket') then 9
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA') then 10
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Software') then 11
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Core') then 12
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Service Desk') then 13
                        --addon with product
                        when a.land_count = 2 and b.base_product in ('HipChat','Marketplace Addon') then 14
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Marketplace Addon') then 15
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Marketplace Addon') then 16
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Marketplace Addon') then 17
                        when a.land_count = 2 and b.base_product in ('JIRA','Marketplace Addon') then 18
                        when a.land_count = 2 and b.base_product in ('Confluence','Marketplace Addon') then 19
                        when a.land_count = 2 and b.base_product in ('Bitbucket','Marketplace Addon') then 20
                        --JIRA family and Confluence only
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Confluence') then 21
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Confluence') then 22
                        when a.land_count = 2 and b.base_product in ('JIRA','Confluence') then 23
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Confluence') then 24
                        -- Hipchat based 3 product combos
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','Confluence') then 25
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA') then 26
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Software') then 27 
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Core') then 28
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk') then 29
                        -- addons with 3
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Confluence','Marketplace Addon') then 30  
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Confluence','Marketplace Addon') then 31
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Confluence','Marketplace Addon') then 32
                        when a.land_count = 3 and b.base_product in ('JIRA','Confluence','Marketplace Addon') then 33
                        when a.land_count = 3 and b.base_product in ('Bitbucket','Confluence','Marketplace Addon') then 34
                        when a.land_count = 3 and b.base_product in ('HipChat','Confluence','Marketplace Addon') then 35
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Bitbucket','Marketplace Addon') then 36
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Bitbucket','Marketplace Addon') then 37
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Bitbucket','Marketplace Addon') then 38
                        when a.land_count = 3 and b.base_product in ('JIRA','Bitbucket','Marketplace Addon') then 39
                        when a.land_count = 3 and b.base_product in ('JIRA Software','HipChat','Marketplace Addon') then 40
                        when a.land_count = 3 and b.base_product in ('JIRA Core','HipChat','Marketplace Addon') then 41
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','HipChat','Marketplace Addon') then 42
                        when a.land_count = 3 and b.base_product in ('JIRA','HipChat','Marketplace Addon') then 43
                        --Bitbucket based 3 product combos
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA','Confluence') then 44
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Software','Confluence') then 45
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Core','Confluence') then 46
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Service Desk','Confluence') then 47
                        -- 4 product combos
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA','Confluence') then 48
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software','Confluence') then 49
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core','Confluence') then 50
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk','Confluence') then 51
                        -- 4 with addon
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','Confluence', 'Marketplace Addon') then 52
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software', 'Marketplace Addon') then 53
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core', 'Marketplace Addon') then 54
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'Marketplace Addon') then 55
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA', 'Marketplace Addon') then 56
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Software', 'Marketplace Addon') then 57
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Core', 'Marketplace Addon') then 58
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Service Desk', 'Marketplace Addon') then 59                
                        -- > 4 product combos                 
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','Confluence') then 60
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Software','JIRA Core','Confluence') then 61
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Core','JIRA Service Desk','Confluence') then 62
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'JIRA Software','Confluence') then 63
                else 64
                end as lander_group,
                count(distinct a.smart_domain)
        from land_product as a
        left join first_product as b on a.smart_domain = b.smart_domain
        group by 1
        order by 1
  