
--first year contribution by platform
with land_year as
(
        select email_domain, financial_year, platform
        from public.sale
        where sale_type = 'New to New'
        group by 1,2,3
)
select  a.platform, 
        a.financial_year, 
        count(distinct a.email_domain) as new_cust,
        sum(b.amount) as total_sales
from land_year as a
left join sale as b on a.email_domain = b.email_domain and a.financial_year = b.financial_year
group by 1,2
order by 1,2