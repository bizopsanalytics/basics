select  financial_year,
        sum(amount)::money as total_bookings,
        count(distinct email_domain) as unique_cust
from sale
--where license_level = 'Full'
group by 1