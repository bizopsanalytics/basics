with prod_count as
(
select  tech_email_domain,
        base_product,
        count(base_product) as prod_num
from license
where base_product in ('JIRA', 'JIRA Software', 'JIRA Service Desk', 'Confluence', 'Bamboo', 'HipChat', 'Bitbucket', 'Crucible', 'FishEye')
and expiry_date > '2016-06-01'
and license_level = 'Full'
group by 1,2
order by 3 desc
)
select  base_product,
        prod_num,
        count(distinct tech_email_domain)
from prod_count
group by 1,2


