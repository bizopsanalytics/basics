select
  financial_year,
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2001-07-01' and '2002-06-30') then amount else 0 end) as "FY2002",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2002-07-01' and '2003-06-30') then amount else 0 end) as "FY2003",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2003-07-01' and '2004-06-30') then amount else 0 end) as "FY2004",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2004-07-01' and '2005-06-30') then amount else 0 end) as "FY2005",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2005-07-01' and '2006-06-30') then amount else 0 end) as "FY2006",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2006-07-01' and '2007-06-30') then amount else 0 end) as "FY2007",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2007-07-01' and '2008-06-30') then amount else 0 end) as "FY2008",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2008-07-01' and '2009-06-30') then amount else 0 end) as "FY2009",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2009-07-01' and '2010-06-30') then amount else 0 end) as "FY2010",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2010-07-01' and '2011-06-30') then amount else 0 end) as "FY2011",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2011-07-01' and '2012-06-30') then amount else 0 end) as "FY2012",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2012-07-01' and '2013-06-30') then amount else 0 end) as "FY2013",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2013-07-01' and '2014-06-30') then amount else 0 end) as "FY2014",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2014-07-01' and '2015-06-30') then amount else 0 end) as "FY2015",
  sum(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2015-07-01' and '2016-06-30') then amount else 0 end) as "FY2016"
from
  sale
where platform = 'Server'
and license_level = 'Full'
group by
  financial_year
order by
  financial_year;
  
  
  --cust count
  select
  financial_year,
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2001-07-01' and '2002-06-30') then email_domain end) as "FY2002",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2002-07-01' and '2003-06-30') then email_domain end) as "FY2003",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2003-07-01' and '2004-06-30') then email_domain end) as "FY2004",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2004-07-01' and '2005-06-30') then email_domain end) as "FY2005",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2005-07-01' and '2006-06-30') then email_domain end) as "FY2006",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2006-07-01' and '2007-06-30') then email_domain end) as "FY2007",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2007-07-01' and '2008-06-30') then email_domain end) as "FY2008",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2008-07-01' and '2009-06-30') then email_domain end) as "FY2009",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2009-07-01' and '2010-06-30') then email_domain end) as "FY2010",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2010-07-01' and '2011-06-30') then email_domain end) as "FY2011",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2011-07-01' and '2012-06-30') then email_domain end) as "FY2012",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2012-07-01' and '2013-06-30') then email_domain end) as "FY2013",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2013-07-01' and '2014-06-30') then email_domain end) as "FY2014",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2014-07-01' and '2015-06-30') then email_domain end) as "FY2015",
  count(case when email_domain in (select email_domain from sale group by email_domain having min(date) between '2015-07-01' and '2016-06-30') then email_domain end) as "FY2016"
from
  sale
where platform = 'Server'
and license_level = 'Full'
group by
  financial_year
order by
  financial_year;